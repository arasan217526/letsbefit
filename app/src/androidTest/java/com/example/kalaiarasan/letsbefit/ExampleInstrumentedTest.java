package com.example.kalaiarasan.letsbefit;

import android.app.Activity;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import static android.support.test.espresso.Espresso.onView;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withInputType;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.kalaiarasan.letsbefit", appContext.getPackageName());
    }
/*
    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule=
            new ActivityTestRule<>(MainActivity.class);
*/
    @Rule
    public ActivityTestRule<MainPage> mainPageActivityTestRule =
            new ActivityTestRule<>(MainPage.class);


    @Test
    public void BMI() {
        onView(withId(R.id.bmi))
                .perform(click());
        onView(withId(R.id.etheight))
                .perform(typeText("168"), closeSoftKeyboard());
        onView(withId(R.id.etweight))
                .perform(typeText("60"), closeSoftKeyboard());
        onView(withId(R.id.btncalculate))
                .perform(click());
    }

    @Test
    public void situp() {
        onView(withId(R.id.imagegym))
                .perform(click());
        onView(withId(R.id.btnsitup))
                .perform(click());
        onView(withId(R.id.startsit))
                .perform(click());
        onView(withId(R.id.tvsit));

    }

    @Test
    public void Flutter() {
        onView(withId(R.id.imagegym))
                .perform(click());
        onView(withId(R.id.btnflutter))
                .perform(click());
        onView(withId(R.id.flutterstart))
                .perform(click());
        onView(withId(R.id.tvflutter));
    }




/**

    @Test
    public void Register(){
        onView(withId(R.id.email))
                .perform(typeText("arasan3@rocketmail.com"),closeSoftKeyboard());
        onView(withId(R.id.Password))
                .perform(typeText("12345"),closeSoftKeyboard());
        onView(withId(R.id.CPassword))
                .perform(typeText("12345"),closeSoftKeyboard());
        onView(withId(R.id.SignUp))
                .perform(click());
        onView(withId(R.id.SignIn))
                .perform(click());
        onView(withId(R.id.email2))
                .perform(typeText("arasan3@rocketmail.com"),closeSoftKeyboard());
        onView(withId(R.id.pw2))
                .perform(typeText("12345"),closeSoftKeyboard());
        onView(withId(R.id.sign2))
                .perform(click());
    }

*/




    }







