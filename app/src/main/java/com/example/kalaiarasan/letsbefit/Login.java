package com.example.kalaiarasan.letsbefit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    EditText q1;
    EditText q2;
    Button w1;
    DatabaseHelper sqliteDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sqliteDatabase = new DatabaseHelper(getApplicationContext(), "LetsBeFit.db", this,1);

        q1= (EditText)findViewById(R.id.email2);
        q2= (EditText)findViewById(R.id.pw2);
        w1= (Button)findViewById(R.id.sign2);

        w1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = q1.getText().toString();
                String pword = q2.getText().toString();

                Boolean checkemailpword = sqliteDatabase.emailpassword(email, pword);
                Intent x = new Intent(Login.this, MainPage.class);

                if (checkemailpword == true) {
                    Toast.makeText(getApplicationContext(), "Sign sucessfully", Toast.LENGTH_SHORT).show();
                    startActivity(x);

                } else
                    Toast.makeText(getApplicationContext(), "wrong email or password", Toast.LENGTH_SHORT).show();
            }

        });

    }
}
