package com.example.kalaiarasan.letsbefit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button a1;
    Button a2;
    EditText b1;
    EditText b2;
    EditText b3;
    DatabaseHelper sqLiteDatabase;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.sqLiteDatabase = new DatabaseHelper(getApplicationContext(), "LetsBeFit.db", this, 1);

        this.b1 = (EditText) findViewById(R.id.email);
        this.b2 = (EditText) findViewById(R.id.Password);
        this.b3 = (EditText) findViewById(R.id.CPassword);
        this.a1 = (Button) findViewById(R.id.SignUp);
        this.a2 = (Button) findViewById(R.id.SignIn);
        this.a2.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, Login.class));
            }

        });
        this.a1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String s1 = MainActivity.this.b1.getText().toString();
                String s2 = MainActivity.this.b2.getText().toString();
                String s3 = MainActivity.this.b3.getText().toString();
                if (!(s1.equals(BuildConfig.FLAVOR) || s2.equals(BuildConfig.FLAVOR))) {
                    if (!s3.equals(BuildConfig.FLAVOR)) {
                        if (!s2.equals(s3)) {
                            Toast.makeText(MainActivity.this.getApplicationContext(), "password do no match", 0).show();
                        } else if (!MainActivity.this.sqLiteDatabase.checkemail(s1).booleanValue()) {
                            Toast.makeText(MainActivity.this.getApplicationContext(), "Email already exists", 0).show();
                        } else if (MainActivity.this.sqLiteDatabase.insert(s1, s2).booleanValue()) {
                            Toast.makeText(MainActivity.this.getApplicationContext(), "registration successfully", 0).show();
                        }
                    }
                }
            }

        });
    }
}