package com.example.kalaiarasan.letsbefit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainPage extends AppCompatActivity implements View.OnClickListener{

    Button Find_nearby_GYM;
    ImageButton imagegym;
    ImageButton bmi;
    Button exit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        initView();
    }

  private void initView(){

      Find_nearby_GYM  = findViewById(R.id.Find_nearby_GYM);
      Find_nearby_GYM.setOnClickListener(this);

      imagegym = (ImageButton)findViewById(R.id.imagegym);
      bmi = (ImageButton)findViewById(R.id.bmi);
      exit = (Button)findViewById(R.id.Exit);

      imagegym.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent q = new Intent(MainPage.this,Exercise.class);
              startActivity(q);
          }
      });

      bmi.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent w = new Intent(MainPage.this,BMI.class);
              startActivity(w);
          }
      });

      exit.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              finish();
              Intent r = new Intent(MainPage.this,Login.class);
              startActivity(r);
          }
      });

  }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.Find_nearby_GYM:
                startActivity(new Intent(MainPage.this,MapsActivity.class));

                break;
        }
    }
}
